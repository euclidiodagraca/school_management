<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('paper') }}/img/logo-small.png">
            </div>
        </a>
        <a href="#" class="simple-text logo-normal">
            {{ Auth::user()->name }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'dashboard') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>{{ __('Painel principal') }}</p>
                </a>
            </li>

            <li class="{{  $elementActive == 'cadeira' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="false" href="#cadeira">
                    <i class="nc-icon nc-book-bookmark"></i>
                    <p>
                        {{ __('Cadeira') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="cadeira">
                    <ul class="nav">
                        <li class="{{ $elementSub == 'cadeira-index' ? 'active' : '' }}">
                            <a href="{{ route('cadeira.index') }}">
                                <span class="sidebar-mini-icon">{{ __('C') }}</span>
                                <span class="sidebar-normal">{{ __(' Cadeiras ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementSub == 'cadeira-create' ? 'active' : '' }}">
                            <a href="{{ route('cadeira.create') }}">
                                <span class="sidebar-mini-icon">{{ __('NC') }}</span>
                                <span class="sidebar-normal">{{ __(' Nova Cadeira ') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="{{  $elementActive == 'curso' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="false" href="#curso">
                    <i class="nc-icon nc-briefcase-24"></i>
                    <p>
                        {{ __('Curso') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="curso">
                    <ul class="nav">
                        <li class="{{ $elementSub == 'curso-index' ? 'active' : '' }}">
                            <a href="{{ route('curso.index') }}">
                                <span class="sidebar-mini-icon">{{ __('C') }}</span>
                                <span class="sidebar-normal">{{ __(' Cursos ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementSub == 'curso-create' ? 'active' : '' }}">
                            <a href="{{ route('curso.create') }}">
                                <span class="sidebar-mini-icon">{{ __('NC') }}</span>
                                <span class="sidebar-normal">{{ __(' Novo curso ') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="{{ $elementActive == 'estudante' ? 'active' : '' }}">
                <a data-toggle="collapse"  href="#estudante" >
                    <i class="nc-icon nc-single-02"></i>
                    <p>
                        {{ __('Estudante') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="estudante">
                    <ul class="nav">
                        <li class="{{ $elementSub == 'estudante-index' ? 'active' : '' }}">
                            <a href="{{ route('estudante.index', 'user') }}">
                                <span class="sidebar-mini-icon">{{ __('E') }}</span>
                                <span class="sidebar-normal">{{ __(' Estudantes ') }}</span>
                            </a>
                        </li>

                        <li class="{{ $elementSub == 'estudante-create' ? 'active' : '' }}">
                            <a href="{{ route('estudante.create') }}">
                                <span class="sidebar-mini-icon">{{ __('NE') }}</span>
                                <span class="sidebar-normal">{{ __(' Novo estudante ') }}</span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="{{ $elementActive == 'grau' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="false" href="#grau">
                    <i class="nc-icon nc-money-coins"></i>
                    <p>
                            {{ __('Grau Grau académico') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="grau">
                    <ul class="nav">
                        <li class="{{ $elementSub == 'grau-index' ? 'active' : '' }}">
                            <a href="{{ route('grau.index', 'user') }}">
                                <span class="sidebar-mini-icon">{{ __('E') }}</span>
                                <span class="sidebar-normal">{{ __(' Grau Grau académico ') }}</span>
                            </a>
                        </li>

                        <li class="{{ $elementSub == 'grau-create' ? 'active' : '' }}">
                            <a href="{{ route('grau.create') }}">
                                <span class="sidebar-mini-icon">{{ __('RP') }}</span>
                                <span class="sidebar-normal">{{ __(' Registar grau ') }}</span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="{{ $elementActive == 'icons' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'icons') }}">
                    <i class="nc-icon nc-diamond"></i>
                    <p>{{ __('Icons') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'map' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'map') }}">
                    <i class="nc-icon nc-pin-3"></i>
                    <p>{{ __('Maps') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'notifications' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'notifications') }}">
                    <i class="nc-icon nc-bell-55"></i>
                    <p>{{ __('Notifications') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'tables' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'tables') }}">
                    <i class="nc-icon nc-tile-56"></i>
                    <p>{{ __('Table List') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'typography' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'typography') }}">
                    <i class="nc-icon nc-caps-small"></i>
                    <p>{{ __('Typography') }}</p>
                </a>
            </li>

        </ul>
    </div>
</div>
