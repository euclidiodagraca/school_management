@if ($message = Session::get('sucesso'))
<div class="alert alert-success alert-dismissible fade show">
    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
        <i class="nc-icon nc-simple-remove"></i>
    </button>
    <span>{{ $message }}</span>
</div>
@endif


@if ($message = Session::get('erro'))
<div class="alert alert-danger alert-dismissible fade show">
    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
        <i class="nc-icon nc-simple-remove"></i>
    </button>
    <span>{{ $message }}</span>
</div>
@endif


@if ($message = Session::get('aviso'))
<div class="alert alert-warning alert-dismissible fade show">
    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
        <i class="nc-icon nc-simple-remove"></i>
    </button>
    <span>{{ $message }}</span>
</div>
@endif


@if ($message = Session::get('info'))
<div class="alert alert-info alert-dismissible fade show">
    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
        <i class="nc-icon nc-simple-remove"></i>
    </button>
    <span>{{ $message }}</span>
</div>
@endif


@if ($errors->any())
<div class="alert alert-primary alert-dismissible fade show">
    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
        <i class="nc-icon nc-simple-remove"></i>
    </button>
    <span>Algo deu errado na sua requisição. Prencha corectamente/ tente novamente</span>
</div>
@endif

