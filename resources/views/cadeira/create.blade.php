@extends('layouts.app', [
'class' => '',
'elementActive' => 'cadeira',
'elementSub' => 'cadeira-create'
])
@section('header')
Registar cadeira
@endsection
@section('content')
<div class="content">
    @include('layouts.flash-messages')
    <div class="wizard-card" data-color="orange" id="wizardForm">
        <form action="{{ route('cadeira.store') }}" method="POST">
            @csrf
            <div class="wizard-header text-left">
                <h4 class="wizard-title">Registar cadeira</h4>
                <p class="font-italic">Esta informação será registada na base de dados da instituição.</p>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group @error('nome') has-danger @enderror">
                        <label class="control-label" for="nome">Nome da cadeira <span
                                class="text-danger">*</span></label>
                        <input type="text" required="true" id="nome"
                            class="form-control border-input @error('nome') form-control-danger @enderror"
                            placeholder="Nome da cadeira" value="{{ old('nome') }}" name="nome">
                        @error('nome')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group @error('codigo') has-danger @enderror">
                        <label class="control-label" for="codigo">Código da cadeira
                            <span class="text-danger">*</span></label>
                        <input type="text" required="true" id="codigo"
                            class="form-control border-input @error('codigo') form-control-danger @enderror"
                            placeholder="Código da cadeira" value="{{ old('codigo') }}" name="codigo">
                        @error('codigo')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group @error('credito') has-danger @enderror">
                        <label class="control-label" for="credito">Créditos <span class="text-danger">*</span></label>
                        <input type="number" required="true" id="credito"
                            class="form-control border-input @error('credito') form-control-danger @enderror"
                            placeholder="Número de créditos" value="{{ old('credito') }}" name="credito">

                        @error('credito')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group @error('ano') has-danger @enderror">
                        <label class="control-label" for="ano">Ano académico <span class="text-danger">*</span></label>
                        <select type="text" required="true" name="ano" id="ano"
                            class="form-control border-input @error('ano') form-control-danger @enderror">
                            <option disabled selected>Seleccionar...</option>
                            <option value="1">1°</option>
                            <option value="2">2°</option>
                            <option value="3">3°</option>
                            <option value="4">4°</option>
                        </select>

                        @error('ano')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group @error('semestre') has-danger @enderror">
                        <label class="control-label" for="semestre">Semestre <span class="text-danger">*</span></label>
                        <select type="text" required="true" name="semestre" id="semestre"
                            class="form-control border-input @error('semestre') form-control-danger @enderror">
                            <option disabled selected>Seleccionar...</option>
                            <option value="1">I°</option>
                            <option value="2">II°</option>
                        </select>

                        @error('semestre')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="form-group @error('cursos') has-danger @enderror">
                        <label class="control-label" for="cursos">Selecione os cursos em que a cadeira é leccionada<span class="text-danger">*</span></label>
                        <select type="text" required="true" id="cursos"
                            class="basic-multiple form-control border-input @error('cursos') form-control-danger @enderror"
                            name="cursos[]" multiple="multiple">
                            <option>Selecci</option>
                            <option>ccc</option>
                            <option>ccc</option>
                            <option>ccc</option>
                            <option>ccc</option>
                            <option>ccc</option>
                            <option>ccc</option>
                            <option>ccc</option>
                            <option>ccc</option>
                        </select>

                        @error('cursos')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="wizard-footer">
                <div class="pull-right">

                    <button type='submit' class='btn btn-finish btn-fill btn-info btn-wd'>Registar</button>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>
@endsection
