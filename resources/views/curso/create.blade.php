@extends('layouts.app', [
'class' => '',
'elementActive' => 'curso',
'elementSub' => 'curso-create'
])
@section('header')
Registar curso
@endsection
@section('content')
<div class="content">
    @include('layouts.flash-messages')
    <div class="wizard-card" data-color="orange" id="wizardForm">
        <form action="{{ route('curso.store') }}" method="POST">
            @csrf
            <div class="wizard-header text-left">
                <h4 class="wizard-title">Registar curso</h4>
                <p class="font-italic">Esta informação será registada na base de dados da instituição.</p>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group @error('nome') has-danger @enderror">
                        <label class="control-label" for="nome">Nome do curso <span class="text-danger">*</span></label>
                        <input type="text" required="true" id="nome"
                            class="form-control border-input @error('nome') form-control-danger @enderror"
                            placeholder="Nome do curso" value="{{ old('nome') }}" name="nome">
                        @error('nome')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group @error('codigo') has-danger @enderror">
                        <label class="control-label" for="codigo">Código do curso
                            <span class="text-danger">*</span></label>
                        <input type="text" required="true" id="codigo"
                            class="form-control border-input @error('codigo') form-control-danger @enderror"
                            placeholder="Código do curso" value="{{ old('codigo') }}" name="codigo">
                        @error('codigo')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group @error('grau') has-danger @enderror">
                        <label class="control-label" for="grau">Grau académico <span
                                class="text-danger">*</span></label>
                        <select type="text" required="true" name="grau" id="grau"
                            class="select-add form-control border-input @error('grau') form-control-danger @enderror">
                            <option selected disabled>Seleccione...</option>
                            <option>...</option>
                        </select>

                        @error('grau')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group @error('preco') has-danger @enderror">
                        <label class="control-label" for="preco">Preço da mensalidade <span
                                class="text-danger">*</span></label>
                        <input type="text" required="true" id="preco"
                            class="form-control border-input @error('preco') form-control-danger @enderror"
                            placeholder="5000" value="{{ old('preco') }}" name="preco">
                        @error('preco')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">

                    <div class="form-group  @error('duracao') has-danger @enderror">
                        <label class="control-label" for="duracao">Duração do curso <span
                                class="text-danger">*</span></label>
                        <input type="text" required="true" id="duracao"
                            class="form-control border-input @error('duracao') form-control-danger @enderror"
                            placeholder="Duração do curso" value="{{ old('duracao') }}" name="duracao">

                        @error('duracao')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group @error('duracao_tempo') has-danger @enderror">
                        <label class="control-label" for="duracao_tempo">Unidade <span
                                class="text-danger">*</span></label>
                        <select type="text" required="true" name="duracao_tempo" id="duracao_tempo"
                            class="form-control border-input @error('duracao_tempo') form-control-danger @enderror">
                            <option disabled selected>Seleccionar...</option>
                            <option value="anos">Anos</option>
                            <option value="meses">Meses</option>
                        </select>

                        @error('duracao_tempo')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group @error('credito') has-danger @enderror">
                        <label class="control-label" for="credito">Créditos <span class="text-danger">*</span></label>
                        <input type="number" required="true" id="credito"
                            class="form-control border-input @error('credito') form-control-danger @enderror"
                            placeholder="Número de créditos" value="{{ old('credito') }}" name="credito">

                        @error('credito')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group @error('preco_cadeira_atraso') has-danger @enderror">
                        <label class="control-label" for="preco_cadeira_atraso">Preço de cadeira em atraso <span
                                class="text-danger">*</span></label>
                        <input type="number" required="true" id="preco_cadeira_atraso"
                            class="form-control border-input @error('preco_cadeira_atraso') form-control-danger @enderror"
                            placeholder="Preço de cadeira em atraso" value="{{ old('preco_cadeira_atraso') }}"
                            name="preco_cadeira_atraso">

                        @error('preco_cadeira_atraso')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="wizard-footer">
                <div class="pull-right">

                    <button type='submit' class='btn btn-finish btn-fill btn-info btn-wd'>Registar</button>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>
@endsection
