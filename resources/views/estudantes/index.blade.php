@extends('layouts.app', [
'class' => '',
'elementActive' => 'estudante',
'elementSub' => 'estudante-index'
])

@section('header')
Lista de estudantes
@endsection

@section('content')
<div class="content">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <div class="card-header">
        <h4 class="wizard-title">Lista dos estudantes</h4>
        <p class="font-italic">Encontre a lista de todos os estudantes da instituição. Podendo filtrar de acordo com a necessidade</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <div class="toolbar">
                        <!--        Here you can write extra buttons/actions for the toolbar              -->
                    </div>
                    <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Apelido</th>
                                <th>Sexo</th>
                                <th>Curso</th>
                                <th>Ano de estudo</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>Hope Fuentes</td>
                                <td>Secretary</td>
                                <td>San Francisco</td>
                                <td>41</td>
                                <td>41</td>

                            </tr>
                            <tr>
                                <td>Vivian Harrell</td>
                                <td>Financial Controller</td>
                                <td>San Francisco</td>
                                <td>62</td>
                                <td>62</td>

                            </tr>

                        </tbody>
                    </table>
                </div><!-- end content-->
            </div><!--  end card  -->
        </div> <!-- end col-md-12 -->
    </div> <!-- end row -->
</div>

@push('scripts')
<link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/buttons.print.min.js') }}"></script>
@endpush

@endsection
