@extends('layouts.app', [
'class' => '',
'elementActive' => 'estudante',
'elementSub' => 'estudante-create'
])

@section('content')
<div class="content">
    @include('layouts.flash-messages')
    <div class="wizard-card" data-color="orange" id="wizardForm">
        <form action="{{ route('estudante.store') }}" method="POST">
            @csrf
            <div class="wizard-header text-left">
                <h4 class="wizard-title">Registar estudante</h4>
                <p class="font-italic">Esta informação será registada na base de dados da instituição.</p>
            </div>

            <div class="wizard-navigation" style="margin-bottom: 1%;">
                <div class="progress-with-circle">
                    <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3"
                        style="width: 21%;"></div>
                </div>
                <ul>
                    <li>
                        <a href="#about" data-toggle="tab">
                            <i class="nc-icon nc-single-02"></i>
                            <span class="sidebar-normal">{{ __(' Dados pessoais ') }}</span>

                        </a>
                    </li>
                    <li>
                        <a href="#account" data-toggle="tab">
                            <i class="nc-icon nc-mobile"></i>
                            <span class="sidebar-normal">{{ __(' Contacto ') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="#address" data-toggle="tab">
                            <i class="nc-icon nc-pin-3"></i>
                            <span class="sidebar-normal">{{ __(' Educação ') }}</span>

                        </a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane" id="about">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group @error('nome') has-danger @enderror">
                                <label class="control-label" for="nome" class="control-label"
                                    for="nome">{{ __('Nome') }} <span class="text-danger">*</span></label>
                                <input type="text" name="nome" id="nome"
                                    class="form-control border-input @error('nome') form-control-danger @enderror"
                                    required="true" value="{{ old('nome') }}" placeholder="Nome">
                                @error('email')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group @error('apelido') has-danger @enderror">
                                <label class="control-label" for="apelido">Apelido <span
                                        class="text-danger">*</span></label>
                                <input type="text" name="apelido" id="apelido"
                                    class="form-control border-input @error('nome') form-control-danger @enderror"
                                    required="true" placeholder="Apelido" value="{{ old('apelido') }}">
                                @error('apelido')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('nr_bi') has-danger @enderror">
                                <label class="control-label" for="nr_bi">Bilhete de identidade <span
                                        class="text-danger">*</span></label>
                                <input type="text" id="nr_bi" name="nr_bi"
                                    class="form-control border-input @error('nr_bi') form-control-danger @enderror"
                                    required="true" value="{{ old('nr_bi') }}" placeholder="Bilhete de identidade">
                                @error('nr_bi')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group @error('local_emissao_bi') has-danger @enderror">
                                <label class="control-label" for="local_emissao_bi">Local de emissão do BI <span
                                        class="text-danger">*</span></label>
                                <input type="text" id="local_emissao_bi" required="true"
                                    class="form-control border-input @error('local_emissao_bi') form-control-danger @enderror"
                                    placeholder="Local de emissão do BI" value="{{ old('local_emissao_bi') }}"
                                    name="local_emissao_bi">
                                @error('local_emissao_bi')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('data_emissao_bi') has-danger @enderror">
                                <label class="control-label" for="data_emissao_bi">Data de emissão <span
                                        class="text-danger">*</span></label>
                                <input type="text" id="data_emissao_bi" required="true"
                                    class="datepicker form-control border-input @error('data_emissao_bi') form-control-danger @enderror"
                                    placeholder="Data de emissão" value="{{ old('data_emissao_bi') }}"
                                    name="data_emissao_bi">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                @error('data_emissao_bi')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('data_validade_bi') has-danger @enderror">
                                <label class="control-label" for="data_validade_bi">Data de validade <span
                                        class="text-danger">*</span></label>
                                <input type="text" id="data_validade_bi"
                                    class="datepicker form-control border-input @error('data_validade_bi') form-control-danger @enderror"
                                    placeholder="Data de validade" value="{{ old('data_validade_bi') }}" required="true"
                                    name="data_validade_bi">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                @error('data_validade_bi')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('data_nascimento') has-danger @enderror">
                                <label class="control-label" for="data_nascimento">Data de nascimento <span
                                        class="text-danger">*</span></label>
                                <input type="text" id="data_nascimento"
                                    class="datepicker form-control border-input @error('data_nascimento') form-control-danger @enderror"
                                    placeholder="Data de nascimento" value="{{ old('data_nascimento') }}"
                                    required="true" name="data_nascimento">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                @error('data_nascimento')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('nacionalidade') has-danger @enderror">
                                <label class="control-label" for="nacionalidade">Nacionalidade <span
                                        class="text-danger">*</span></label>
                                <select type="text" name="nacionalidade" id="nacionalidade"
                                    class="select-add form-control border-input @error('nacionalidade') form-control-danger @enderror"
                                    required="true">
                                    <option selected disabled>Choose...</option>
                                    <option>...</option>
                                </select>

                                @error('nacionalidade')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('naturalidade') has-danger @enderror">
                                <label class="control-label" for="naturalidade">Naturalidade <span
                                        class="text-danger">*</span></label>
                                <select type="text" required="true" name="naturalidade" id="naturalidade"
                                    class="select-add-creation form-control border-input @error('naturalidade') form-control-danger @enderror">
                                    <option selected disabled>Choose...</option>
                                    <option>...</option>
                                </select>

                                @error('naturalidade')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="sexo">Sexo <span class="text-danger">*</span></label>
                            <div class="form-group @error('sexo') has-danger @enderror">

                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="sexo" id="femenino"
                                            value="Femenino"> Femenino
                                        <span class="form-check-sign"></span>
                                        <span class="text-danger">*</span></label>
                                </div>
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="sexo" id="masculino"
                                            value="Masculino"> Masculino
                                        <span class="form-check-sign"></span>
                                        <span class="text-danger">*</span></label>
                                </div>

                                @error('sexo')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('estado_civil') has-danger @enderror">
                                <label class="control-label" for="estado_civil">Estado civil <span
                                        class="text-danger">*</span></label>
                                <select type="text" required="true" name="estado_civil" id="estado_civil"
                                    class="form-control border-input @error('estado_civil') form-control-danger @enderror">
                                    <option disabled selected disabled>Seleccione...</option>
                                    <option value="Casado">Casado</option>
                                    <option value="Divorciado">Divorciado</option>
                                    <option value="Solteiro">Solteiro</option>
                                </select>

                                @error('estado_civil')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group @error('ocupacao') has-danger @enderror">
                                <label class="control-label" for="ocupacao">Ocupação <span
                                        class="text-danger">*</span></label>
                                <input type="text" required="true" id="ocupacao"
                                    class="form-control border-input @error('ocupacao') form-control-danger @enderror"
                                    placeholder="Ocupação" value="{{ old('ocupacao') }}" name="ocupacao">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                @error('ocupacao')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                {{-- End of Personal Details  --}}
                {{-- Start With contact details  --}}
                <div class="tab-pane" id="account">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group @error('email') has-danger @enderror">
                                <label class="control-label" for="email">E-mail <span
                                        class="text-danger">*</span></label>
                                <input type="email" id="email"
                                    class="form-control border-input @error('email') form-control-danger @enderror"
                                    required="true" placeholder="E-mail" value="{{ old('email') }}" name="email">
                                @error('email')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('telefone') has-danger @enderror">
                                <label class="control-label" for="telefone">Telefone <span
                                        class="text-danger">*</span></label>
                                <input type="tel" id="telefone" required="true"
                                    class="form-control border-input @error('telefone') form-control-danger @enderror"
                                    placeholder="Telefone" value="{{ old('telefone') }}" name="telefone">
                                @error('telefone')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('telefone_alternativo') has-danger @enderror">
                                <label class="control-label" for="telefone_alternativo">Telefone alternativo <span
                                        class="text-danger">*</span></label>
                                <input type="tel" id="telefone_alternativo" required="true"
                                    class="form-control border-input @error('telefone_alternativo') form-control-danger @enderror"
                                    placeholder="Telefone alternativo" value="{{ old('telefone_alternativo') }}"
                                    name="telefone_alternativo">
                                @error('telefone_alternativo')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group @error('morada') has-danger @enderror">
                                <label class="control-label" for="morada">Morada <span
                                        class="text-danger">*</span></label>
                                <select type="text" required="true" name="morada" id="morada"
                                    class="select-add form-control border-input @error('morada') form-control-danger @enderror">
                                    <option selected disabled>Choose...</option>
                                    <option>...</option>
                                </select>

                                @error('morada')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('localidade') has-danger @enderror">
                                <label class="control-label" for="localidade">Localidade <span
                                        class="text-danger">*</span></label>
                                <select type="text" required="true" name="localidade" id="localidade"
                                    class="select-add form-control border-input @error('localidade') form-control-danger @enderror">
                                    <option selected disabled>Choose...</option>
                                    <option>...</option>
                                </select>

                                @error('localidade')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('pais') has-danger @enderror">
                                <label class="control-label" for="pais">Pais <span class="text-danger">*</span></label>
                                <select type="text" required="true" name="pais" id="pais"
                                    class="select-add form-control border-input @error('pais') form-control-danger @enderror">
                                    <option selected disabled>Choose...</option>
                                    <option>...</option>
                                </select>

                                @error('pais')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group @error('codigo_postal') has-danger @enderror">
                                <label class="control-label" for="codigo_postal">Código postal <cite
                                        class="text-danger text-">Opcional</cite></label>
                                <input type="number" id="codigo_postal"
                                    class="form-control border-input @error('codigo_postal') form-control-danger @enderror"
                                    placeholder="1100" value="{{ old('codigo_postal') }}" name="codigo_postal">
                                @error('codigo_postal')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End of Personal Details  --}}
                {{-- Start With contact details  --}}
                <div class="tab-pane" id="address">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group @error('qualificacao_previa') has-danger @enderror">
                                <label class="control-label" for="qualificacao_previa">Qualificação prévia <span
                                        class="text-danger">*</span></label>
                                <input type="text" required="true" id="qualificacao_previa"
                                    class="form-control border-input @error('qualificacao_previa') form-control-danger @enderror"
                                    placeholder="12ª classe, licenciatura, ou outra"
                                    value="{{ old('qualificacao_previa') }}" name="qualificacao_previa">
                                @error('qualificacao_previa')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group @error('instituicao_ensino_medio') has-danger @enderror">
                                <label class="control-label" for="instituicao_ensino_medio">Instituição de educação
                                    <span class="text-danger">*</span></label>
                                <input type="text" required="true" id="instituicao_ensino_medio"
                                    class="form-control border-input @error('instituicao_ensino_medio') form-control-danger @enderror"
                                    placeholder="Nome completo da escola, colegio ou outra"
                                    value="{{ old('instituicao_ensino_medio') }}" name="instituicao_ensino_medio">
                                @error('instituicao_ensino_medio')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group @error('curso_id') has-danger @enderror">
                                <label class="control-label" for="curso_id">Curso <span
                                        class="text-danger">*</span></label>
                                <select type="text" required="true" name="curso_id" id="curso_id"
                                    class="basic-single form-control border-input @error('curso_id') form-control-danger @enderror">
                                    <option selected disabled>Choose...</option>
                                    <option>...</option>
                                </select>

                                @error('curso_id')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group @error('data_conclusao') has-danger @enderror">
                                <label class="control-label" for="data_conclusao">Data de conclusão <span
                                        class="text-danger">*</span></label>
                                <input type="text" required="true" id="data_conclusao"
                                    class="datepicker form-control border-input @error('data_conclusao') form-control-danger @enderror"
                                    placeholder="Data de conclusão" value="{{ old('data_conclusao') }}"
                                    name="data_conclusao">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                @error('data_conclusao')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group @error('localidade_morada_educacao') has-danger @enderror">
                                <label class="control-label" for="localidade_morada_educacao">Localidade <span
                                        class="text-danger">*</span></label>
                                <input type="text" required="true" id="localidade_morada_educacao"
                                    class="form-control border-input @error('localidade_morada_educacao') form-control-danger @enderror"
                                    placeholder="Localidade" value="{{ old('localidade_morada_educacao') }}"
                                    name="localidade_morada_educacao">
                                @error('localidade_morada_educacao')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group @error('pais_estudo') has-danger @enderror">
                                <label class="control-label" for="pais_estudo">País <span
                                        class="text-danger">*</span></label>
                                <select type="text" required="true" name="pais_estudo" id="pais_estudo"
                                    class="select-add form-control border-input @error('pais_estudo') form-control-danger @enderror">
                                    <option selected disabled>Choose...</option>
                                    <option>...</option>
                                </select>

                                @error('pais_estudo')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group @error('grau') has-danger @enderror">
                                <label class="control-label" for="grau">Grau académico a frequentar <span
                                        class="text-danger">*</span></label>
                                <select type="text" required="true" name="grau" id="grau"
                                    class="form-control border-input @error('grau') form-control-danger @enderror">
                                    <option selected disabled>Choose...</option>
                                    <option>...</option>
                                </select>

                                @error('grau')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wizard-footer">
                <div class="pull-right">
                    <input type='button' class='btn btn-next btn-fill btn-warning btn-wd' name='next' value='Próximo' />
                    <button type='submit' class='btn btn-finish btn-fill btn-info btn-wd'
                        name='finish'>Registar</button>
                </div>

                <div class="pull-left">
                    <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>
@endsection
