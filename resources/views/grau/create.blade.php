@extends('layouts.app', [
'class' => '',
'elementActive' => 'grau',
'elementSub' => 'grau-create'
])
@section('header')
Preços da instituição
@endsection
@section('content')
<div class="content">
    @include('layouts.flash-messages')
    <div class="wizard-card" data-color="orange" id="wizardForm">
        <form action="{{ route('grau.store') }}" method="POST">
            @csrf
            <div class="wizard-header text-left">
                <h4 class="wizard-title">Registar grau académico</h4>
                <p class="font-italic">Esta informação será registada na base de dados da instituição.</p>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group @error('nome') has-danger @enderror">
                        <label class="control-label" for="nome">Nome do grau académico <span
                                class="text-danger">*</span></label>
                        <input type="text" required="true" id="nome"
                            class="form-control border-input @error('nome') form-control-danger @enderror"
                            placeholder="Nome do grau académico " value="{{ old('nome') }}" name="nome">
                        @error('nome')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group @error('preco_matricula') has-danger @enderror">
                        <label class="control-label" for="preco_matricula">Preço de matricula <span
                                class="text-danger">*</span></label>
                        <input type="text" required="true" id="preco_matricula"
                            class="form-control border-input @error('preco_matricula') form-control-danger @enderror"
                            placeholder="Preço de matricula" value="{{ old('preco_matricula') }}"
                            name="preco_matricula">
                        @error('preco_matricula')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group @error('preco_inscricao') has-danger @enderror">
                        <label class="control-label" for="preco_inscricao">Preço de inscrição
                            <span class="text-danger">*</span></label>
                        <input type="text" required="true" id="preco_inscricao"
                            class="form-control border-input @error('preco_inscricao') form-control-danger @enderror"
                            placeholder="Código do curso" value="{{ old('preco_inscricao') }}" name="preco_inscricao">
                        @error('preco_inscricao')
                        <span class="invalid-feedback" style="display: block;" role="alert">
                            <strong>{{ $message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

            </div>

            <div class="wizard-footer">
                <div class="pull-right">

                    <button type='submit' class='btn btn-finish btn-fill btn-info btn-wd'>Registar</button>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>
@endsection
