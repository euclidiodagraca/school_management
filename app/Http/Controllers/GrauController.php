<?php

namespace App\Http\Controllers;

use App\Models\Grau;
use Illuminate\Http\Request;

class GrauController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('grau.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Grau::create($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Grau  $grau
     * @return \Illuminate\Http\Response
     */
    public function show(Grau $grau)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Grau  $grau
     * @return \Illuminate\Http\Response
     */
    public function edit(Grau $grau)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Grau  $grau
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grau $grau)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Grau  $grau
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grau $grau)
    {
        //
    }
}
