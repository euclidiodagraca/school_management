<?php

namespace App\Http\Controllers;

use App\Cadeira;
use App\Models\Curso;
use App\Models\Grau;
use Illuminate\Http\Request;

class CadeiraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $graus = Grau::all();
        if (count($graus) <= 0) {
            return redirect(route('home'))->with('erro', 'Não existe nenhum grau académico registado. Comece por registar os graus académicos ');
        }
        $cursos = Curso::all();
        return view('cadeira\create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cadeira  $cadeira
     * @return \Illuminate\Http\Response
     */
    public function show(Cadeira $cadeira)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cadeira  $cadeira
     * @return \Illuminate\Http\Response
     */
    public function edit(Cadeira $cadeira)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cadeira  $cadeira
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cadeira $cadeira)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cadeira  $cadeira
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cadeira $cadeira)
    {
        //
    }
}
