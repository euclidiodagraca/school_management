<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curso extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'nome', 'codigo', 'grau', 'preco', 'duracao', 'duracao_tempo',
        'credito', 'preco_cadeira_atraso'
    ];
}
