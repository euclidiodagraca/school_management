<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estudante extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'nome', 'apelido', 'nr_bi', 'local_emissao_bi',
        'data_emissao_bi', 'data_validade_bi',
        'data_nascimento', 'nacionalidade', 'naturalidade', 'sexo', 'estado_civil',
        'ocupacao', 'email', 'telefone', 'telefone_alternativo', 'morada', 'localidade', 'pais', 'codigo_postal', 'qualificacao_previa', 'instituicao_ensino_medio',
        'curso_id', 'data_conclusao',
        'localidade_morada_educacao',
        'pais_estudo', 'grau', 'finish',
    ];
    //timestamp
    public $timestamps = true;
}
