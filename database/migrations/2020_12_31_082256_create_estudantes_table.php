<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('apelido');
            $table->string('nr_bi');
            $table->string('local_emissao_bi');
            $table->string('data_emissao_bi');
            $table->string('data_validade_bi');
            $table->string('data_nascimento');
            $table->string('nacionalidade');
            $table->string('naturalidade');
            $table->enum('sexo', ['Masculino', 'Feminino', 'Outro']);
            $table->enum('estado_civil', ['Casado', 'Divorciado', 'Solteiro']);
            $table->string('ocupacao');
            $table->string('email');
            $table->string('telefone');
            $table->string('telefone_alternativo');
            $table->string('morada');
            $table->string('localidade');
            $table->string('pais');
            $table->integer('codigo_postal')->nullable();
            $table->string('qualificacao_previa');
            $table->string('instituicao_ensino_medio');
            $table->foreignId('curso_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('data_conclusao');
            $table->string('localidade_morada_educacao');
            $table->string('pais_estudo');
            $table->string('grau');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudantes');
    }
}
