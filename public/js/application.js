
$(document).ready(function () {
    $(".select-add-creation").select2({
        tags: true
    });
});

$(document).ready(function () {
    $(".select-add").select2({
        tags: true
    });
});

$('.basic-single').select2({
    placeholder: 'Seleccione uma opção'
});

$('.basic-multiple').select2({
    placeholder: 'Seleccione'
});

// $(document).ready(function () {
//     $('.basic-multiple').select2();
// });

//DataTable
$(document).ready(function () {
    $('#datatable').DataTable({
        dom: 'Bflrtip',
        buttons: [
            { extend: 'print', text: 'Exportar PDF',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '13pt')


                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', '10pt');

                    $(win.document.body).find('h1')
                        .addClass('compact')
                        .css('font-size', '14pt');

                    $(win.document.body).find('h1')
                        .addClass('compact')
                        .css('font-weight', 'bold');
                },
                header: true,
                title: 'Sistema',
                // portrait
            },

        ],

        "pagingType": "full_numbers",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "Todos"]
        ],
        responsive: true,
        "language": {
            "sEmptyTable": "Não foi encontrado nenhum registo",
            "sLoadingRecords": "A carregar...",
            "sProcessing": "A processar...",
            "sLengthMenu": "Mostrar _MENU_ registos",
            "sZeroRecords": "Não foram encontrados resultados",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
            "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
            "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
            "sSearch": "Procurar:",
            "oPaginate": {
                "sFirst": "Primeiro",
                "sPrevious": "Anterior",
                "sNext": "Seguinte",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        customize: function (doc) {
            $(doc.document.body).find('h1').css('font-size', '11pt');
            $(doc.document.body).find('h1').css('text-align', 'center');
        }


    });

    var table = $('#datatable').DataTable();

    // Edit record
    table.on('click', '.edit', function () {
        $tr = $(this).closest('tr');

        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function () {
        alert('You clicked on Like button');
    });
});
